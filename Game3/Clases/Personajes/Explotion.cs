﻿using Game.Clases.Sprites;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game.Clases.Personajes
{
    class Explotion : SpriteMultiFrame

    {
        static SoundEffect Sound;
        TimeSpan lastframe;

        public Explotion(Point location)
        {
            if (Sound == null)
                Sound = Game1.CurrentGame.Content.Load<SoundEffect>
                            ("Sonidos/Explosion");

            Location = location;
            for (int i = 0; i < 12; i++)
            {
                
                frames.Add(i, new Rectangle(i * 132, 0, 132, 132));
                
            }
            
            imagen = Game1.CurrentGame.Content.Load<Texture2D>("Imagenes/animacionExplosion");

        }
        
        bool SoundPlayed = false;
        
        public override void Update(GameTime gameTime)
        {
            if (SoundPlayed == false)
            {
                SoundPlayed = true;
                Sound.Play();
            }
            if (gameTime.TotalGameTime.Subtract(lastframe).Milliseconds > 100)
            {
                lastframe = gameTime.TotalGameTime;
                currentFrame++;
                if (currentFrame > 11)
                {
                                       
                    Game1.CurrentGame.NewSprites.Add(this);
                }
            }
        }


        
    }
}
