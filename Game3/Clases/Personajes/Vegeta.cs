﻿using Game.Clases.Sprites;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Clases.Personajes
{
    public class Vegeta : MultiImageSprite
      
    {
        int speed = 5;
        TimeSpan lastbullet;
        public int health = 500;
        
        public int cantidad = 10;
             
        
        

        public int Health
        {
            get { return health; }
            set
            {
                health = value;
            }
        }
                
        
        public Vegeta()
        {
            
            imagenes.Add(0, Game1.CurrentGame.Content.Load<Texture2D>("Imagenes/vegeta"));
            Frame = new Rectangle(100,100,100,100);
                        
        }

        

        public override void Update(GameTime gameTime)
        {
            
            int x = Frame.X;
            int y = Frame.Y;
            
            if (Keyboard.GetState().IsKeyDown(Keys.Space) &&
                gameTime.TotalGameTime.Subtract(lastbullet).Milliseconds
                >= 200)
                
            {
                Game1.CurrentGame.NewSprites.Add(new Bullet(Location));
                lastbullet = gameTime.TotalGameTime;
            }
            
            if (Keyboard.GetState().IsKeyDown(Keys.M) &&
               gameTime.TotalGameTime.Subtract(lastbullet).Milliseconds
               >= 400 && cantidad > 0)
            {
                cantidad -= 1;
                Game1.CurrentGame.NewSprites.Add(new Bullet2(Location));
                lastbullet = gameTime.TotalGameTime;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Left))
                x -= speed;
            if (Keyboard.GetState().IsKeyDown(Keys.Right))
                x += speed;
            if (Keyboard.GetState().IsKeyDown(Keys.Up))
                y -= speed;
            if (Keyboard.GetState().IsKeyDown(Keys.Down))
                y += speed;

            ValidateLocation(ref x, ref y);
            Frame = new Rectangle(x, y, 140,100);

            foreach (var item in Game1.CurrentGame.Sprites)
            // foreach (var item in Game1.CurretGame.NewSprites2)
            {
                 if (item is Cell)
                {
                    Cell enemy = item as Cell;                                                      
                     
                     
                     if (Frame.Intersects (enemy.Frame))   
                     {
                         
                             this.Health = this.Health - 40;

                             if (this.Health <= 40)
                             {
                                 Game1.CurrentGame.NewSprites.Add(this);
                                 Game1.CurrentGame.NewSprites.Add(new Explotion(Location));
                                 Game1.life = Game1.life - 1;

                                 if (Game1.life > 0)
                                 {

                                     Game1.CurrentGame.NewSprites.Add(new Vegeta());

                                 }
                                 else
                                 {
                                     //Game1.CurrentGame.NewSprites.Add(nave);
                                     Game1.CurrentGame.NewSprites.Add(this);
                                     Game1.CurrentGame.NewSprites.Add(new Explotion(Location));
                                     Game.Clases.Juego.EnemyFactory.FinGame = true;    
                                     

                                 } 
                             }
                             else
                             {
                                 Game1.CurrentGame.NewSprites.Add(enemy);
                                 Game1.CurrentGame.NewSprites.Add(new Explotion(Location));
                             
                             }
 
                         }
                         
                 }
            }    
        }
            

            protected override void ValidateLocation(ref int x, ref int y)
        {
                               
            if (x < 0)
                x = 0;
            if (y < 0)
                y = 0;
            if (x > Game1.CurrentGame.GraphicsDevice.Viewport
                .Width - 50)
                x = Game1.CurrentGame.GraphicsDevice.Viewport
                    .Width - 50;
            if (y > Game1.CurrentGame.GraphicsDevice.Viewport
                .Height - 50)
                y = Game1.CurrentGame.GraphicsDevice.Viewport
                    .Height - 50;
            
            //sprintePosition = spriteVelocity + sprintePosition;
            }
     
        public override void Draw(GameTime gameTime)
            {
                if (Health > 50)
                {
                    Game1.CurrentGame.spriteBatch.DrawString(MainFont,
                        ("Energia " + Health.ToString()), new Vector2(20, 20), Color.Yellow);
                    base.Draw(gameTime);
                }

                else
                {
                    Game1.CurrentGame.spriteBatch.DrawString(MainFont,
                        ("Energia " + Health.ToString()), new Vector2(20, 20), Color.Red);
                    base.Draw(gameTime);
                                
                }
                
                Game1.CurrentGame.spriteBatch.DrawString(MainFont,
                  ("Puntaje "+  Game1.Score.ToString()), new Vector2(600, 20), Color.White);
                base.Draw(gameTime);

                //Game1.CurrentGame.spriteBatch.DrawString(MainFont,
                  //    ("Fusiles " + cantidad.ToString()), new Vector2(20, 450), Color.Yellow);
                //base.Draw(gameTime);
                
                Game1.CurrentGame.spriteBatch.DrawString(MainFont,
                         ("Vidas " + Game1.life.ToString()), new Vector2(20, 50), Color.Silver);
                base.Draw(gameTime);

            
        }
    }
 
    
    
}
