﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game.Clases.Juego
{
    public abstract class Updateable
    {
        public abstract void Update(GameTime gametime);
        public static Random rand { get; private set; }

        public Updateable()
        {
            if (rand == null)
                rand = new Random();
        }    
    }
}
