﻿using Game.Clases.Sprites;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using Microsoft.Xna.Framework.Audio;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Clases.Personajes
{
    public class BulletEnemy : MultiImageSprite
    {
        int speed = -10;
        static SoundEffect Sound;
        static SoundEffect SoundVida;

       public BulletEnemy(Point location)
        {
            Location= location;
            
           if (Sound == null)
                Sound = Game1.CurrentGame.Content.
                    Load<SoundEffect>("Sonidos/Disparo");
            
            imagenes.Add(0,
                Game1.CurrentGame.Content.Load<Texture2D>("Imagenes/laser"));
         }

        bool SoundPlayed = false;
        
        public override void Update(GameTime gameTime)
        {
            if (SoundPlayed == false)
            {
                SoundPlayed = true;
                Sound.Play();
            }

            if (SoundPlayed == false)
            {
                SoundPlayed = true;
                SoundVida.Play();
            } 
            
            int x = Frame.X;
            int y = Frame.Y;
            x += speed;
                        
            if (x > Game1.CurrentGame.GraphicsDevice.Viewport.Width * 2)
            {
                Game1.CurrentGame.NewSprites.Add(this);
            }
            
            Frame = new Rectangle(x, y, 75, 75);

            foreach (var item in Game1.CurrentGame.Sprites)
            {
                if (item is Vegeta)
                {
                    Vegeta nave = item as Vegeta;
                    if (Frame.Intersects(nave.Frame))
                    {
                        int healt;
                        healt = nave.Health;
                        healt = healt - 20;
                        nave.Health = healt;
                        //nave.Health -= 10;
                        
                        if (nave.Health <= 0 )
                        {
                            Game1.CurrentGame.NewSprites.Add(nave);
                            Game1.CurrentGame.NewSprites.Add(this);
                            Game1.CurrentGame.NewSprites.Add(new Explotion(Location));
                            Game1.life = Game1.life - 1;
                            if (Game1.life <= 0)
                            {

                                Game.Clases.Juego.EnemyFactory.FinGame = true;
                                Game1.FinGame = true;    
                                
                            }
                            else
                            {
                                Game1.CurrentGame.NewSprites.Add(new Vegeta());
                            
                            } 
                        }
                        else
                        {
                            Game1.CurrentGame.NewSprites.Add(this);
                        }
                        
                    }

                }

            }
        }


    }
}
