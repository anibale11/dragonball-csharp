﻿using Game.Clases.Juego;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Clases.Sprites
{
    public abstract class SpritesPrueba : Updateable

    {
        public Rectangle Frame { get; protected set; }
        public Rectangle Frame1 { get; protected set; }

        public Point LocationNave
        {
            get { return Frame.Location; }
            set { Frame = new Rectangle(value, Frame.Size); }
        }

        public Point Location2
        {
            get { return Frame1.Location; }
            set { Frame1 = new Rectangle(value, Frame1.Size); }
        }

        public abstract void Draw(GameTime gameTime);  
    }
}
